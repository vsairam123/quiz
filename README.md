# Quiz

This is an online quiz-taking application that allows the user to take a quiz on a particualar topic and assess his knowledge on the topic. This is a full fledged application having seperate frontend, backend and database layers.  Technology stack used for this application : 
> **Frontend**: React

> **Backend**: Flask 

> **Database**: MongoDb 

The application is hosted on Heroku : [https://quiz-udemy-fend.herokuapp.com/](https://quiz-udemy-fend.herokuapp.com/) 
Please feel free to checkout the application.

# Features 
Each quiz can have any number of questions. The questions are however, multiple-choice only and the question can have any number of choices. At the end of the quiz the user has to submit the quiz along with his name to see the score.  On the score page there is link to the leaderboard page where he can see the top 10 performers of the quiz. 


## Assumptions

For now, the application is limited to just one quiz whose id is hard coded in the application. With slight modification it can be extended to include other quizes as well. 
The application doesn't have an admin interface, all the questions and choices are directly configured on the database.  
It also doesn't consider user authenticion too.

## Achitectural Design and Tech Choices:
**Frontend**: 
The frontend is developed using React.  The following routes are defined for the application:
> '/' : the home page where there would be a button to start the quiz. *Could include options for selecting the quiz/ test instructions

> 'quiz/quizid' : Fetches the particular quiz identified by the  quizid (here quizname is used as the id) and displays the questions one at a time. At the end of the quiz the score would be displayed.

> '/standings': Displays the leaderboard for the particular quiz. 

**Backend**:
The backend is developed using Flask. Since the quiz has minimal features,  using Flask, which is a lightweight python web framework, for faster developement seemed suitable for this task instead of using heavier framework like Django. 
The backend exposes REST apis  for:
> Fetching the questions by the quizname/ quizid

> Posting the results of the user and his score to the database. 

> Retreiving the leaderboard for the quiz. It returns only the top 10 scores. 

**Database**:
The backend connects to the MongoDb Nosql database using PyMongo driver.  The database contains two collections:
> 'questions': it contains the a documents for each quiz.  Each quiz has a quiz identifier folllowed by the list of questions with choices and answers

> 'results': it stores the quiz results as per username and quizname combination.

# Note:

The frontend and backend are deployed seperately using Heroku's git integration. For viewing purposes, both the frontend and backend are put together. The flask server folder contains the backend code. 

# Future Work
As future work, more time can be spent on extending the features and improving the design to scale the application to large number of users. Some of the features can include:
> Generate unique urls for each quiz and send out the link to different user.

> Admin interface for creating/editing the quiz.

> Link user account to the application.  

> Add timers so that it can be taken in a timed setting.
