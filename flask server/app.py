import flask 
import os
from flask import request, jsonify
from flask_cors import CORS
from pymongo import MongoClient 
from bson.json_util import dumps
import datetime

app = flask.Flask(__name__)
app.config["debug"]=True 
cors = CORS(app)

client= MongoClient('mongodb+srv://todoAdmin:todoPassword@testcluster-xxagk.mongodb.net/test?retryWrites=true&w=majority')
db=client.quiz


@app.route('/',methods=['GET'])
def home():
    return "<h1>Healthcheck successful</h1>"

@app.route('/api/getQuestions/<string:quizname>',methods=['GET'])
def getQuestions(quizname):
    collection = db.questions
    questions = collection.find({"quizname":quizname})
    print(questions)
    if questions.count()==0:
        return jsonify({"quizname":quizname,"questions":None})

    return dumps(questions)

@app.route('/api/results',methods=['POST','GET'])
def results():
    collection = db.results
    if request.method=='GET':
        return dumps(collection.find({},limit=10).sort([('score',-1)]))
    else: 
        req = request.get_json()
        print(req)
        # req['time']=datetime.datetime.now()
        collection.insert_one(req)
        return jsonify({"message":"Inserted Successfully!!"})

if __name__ == "__main__":
    app.run()