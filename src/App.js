import React from 'react';
import './App.css';


function App() {

  
  return (
    <div className="App">
      <div className="App-header">
        <p>
          Click on Begin to start the Quiz:
        </p>
        
        <a
          className="waves-effect waves-light btn-large"
          href="/quiz/quiz1/"
          rel="noopener noreferrer"
        >
          Begin Quiz!!!
        </a>
      </div>
    </div>
  );
}

export default App;
