import React from 'react';
import axios from 'axios';

const API_URL = "https://quiz-udemy-backend.herokuapp.com/";


class Leaderboard extends React.Component{
    constructor(props){
        super(props);
        this.state={
            tabledata : ''
        }
    }

    componentDidMount(){
        const url = API_URL+"api/results";
        axios.get(url).then((response)=>{
            let data = response.data;
            console.log(data)
            this.setState({tabledata:data});
        });
    }

    render(){
        return (
            <div className="App">
                <h3>Leaderboard</h3>
                <table className="striped bordered centered">
                    <thead>
                        <tr>
                            <th>Quiz</th>
                            <th>User</th>
                            <th>Score</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.tabledata && this.state.tabledata.map((x,index) => (
                            <tr key={index} >
                                <td>{x.quizname}</td>
                                <td>{x.username}</td>
                                <td>{x.score}</td>
                            </tr>
                        )

                        )}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Leaderboard;