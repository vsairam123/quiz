const qbank = [{
    id:1,
    question:"A card is selected at random from a standard 52 card deck. Assuming all cards are equally likely to be selected, what is the probability that a king was selected given that a red card was selected?",
    options:["1/2","1/26","1/13","1/4"],
    correct: 2
},{
    id:2,
    question:"Suppose two cards are drawn from a standard 52 card deck without replacement (which means when we draw a card, we don't put it back). Assuming all cards are equally likely to be selected, what is the probability that both cards are red?",
    options:["25/102","13/15","25/2704","1/4"],
    correct:0
},{
    id:3,
    question:"Suppose two cards are drawn from a standard 52 card deck without replacement (which means when we draw a card, we don't put it back). Assuming all cards are equally likely to be selected, what is the probability that the second card is not an ace given that the first card is not an ace?",
    options:["16/17","1/4","1/13","47/51"],
    correct:3
},{
    id:4,
    question:"The command that will list all files in long format in current working directory including hidden files...",
    options:["ls -l","ls -a","ls -la","dir"],
    correct:2
},{
    id:5,
    question:"In the Context of Web Application Attacks, Cross-Site Scripting (XSS) is a technique where",
    options:["A user writes a script intended to be run by another user's web browser ","A user writes a script intended to be run by a website's administrator ","A user tricks a server into running a script that was only tested on a different server","A user writes a script intended to be run by another user's web server"],
    correct:0
}];

export default Promise.resolve(qbank);