import React from 'react';

class Question extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            no :'',
            question: '',
            options: '',
            selectedOption:-1,
            numQues:0
        }
        this.nextQues=this.nextQues.bind(this);
    }

    componentDidMount(){
        this.setState({
            no:this.props.qno,
            question: this.props.bank.question,
            options: this.props.bank.options,
            selectedOption:-1,
            numQues:this.props.numQuestions
        });
    }

    componentWillReceiveProps(newProps){
        this.setState({
            no:newProps.qno,
            question: newProps.bank.question,
            options: newProps.bank.options,
            selectedOption:-1,
            numQues:newProps.numQuestions
        });
    }

    selectOption= (option) => {
        this.setState({selectedOption:option})
    }

    nextQues= (option) =>{
        this.props.nextQuestion(option);
        // this.render();
    }

    render(){
        return (
            <div className="container">
                Q{this.state.no+1}:
                {this.state.question}

                <div className = "row collection">
                    {this.state.options && this.state.options.map((opt,index) => (
                        <a key={index} 
                        className={"collection-item" + (this.state.selectedOption===index ? ' active':'')}
                        onClick={()=>this.selectOption(index)}>
                        {String.fromCharCode(65+index)}. {opt}
                        </a>
                        // {opt}
                    )
                    )}
                </div>
                <div className="navigation">
                    {this.state.selectedOption!== -1 && 
                    <a className="waves-effect waves-light btn" 
                    onClick={() => this.nextQues(this.state.selectedOption)}>
                        {/* {this.state.numQues === this.state.no+1 ? "Submit" :"Next" } */}
                        Next
                    </a>}
                </div>
            </div>
        );
    }
}

export default Question;