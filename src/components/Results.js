import React from 'react';
import axios from 'axios';

const API_URL = "https://quiz-udemy-backend.herokuapp.com/";

class Results extends React.Component{
    constructor(props){
        super(props);
        this.state={
            questions : props.bank,
            answers: props.selectedAnswers,
            username:'',
            showSubmitForm:true
        };

    }

    calculateScore = () =>{
        let score =0;
        for(let i = 0 ; i< this.state.questions.length;i++){
            if(this.state.questions[i].correct===this.state.answers[i]){
                score++;
            }
        }
        return score;
    }
    submitResults =() =>{
        const url = API_URL+"api/results";
        let data ={};
        data["username"]=this.state.username;
        data["quizname"]="quiz1"
        data["score"]=this.calculateScore();
        data["answers"]=this.state.answers;

        axios.post(url,data).then(response=>{
            console.log(response);
            this.setState({showSubmitForm:false})
        });
    }

    updateUserName = (event) =>  this.setState({username:event.target.value});

    render(){
        return (
            <div className="App">
                <h1>Great Job !!!</h1>
                
                {this.state.showSubmitForm ?
                    <div>
                    <p>Enter your Name to view the results:</p>
                    <div className="input-field">
                        <input placeholder="Name" 
                            id="username" 
                            type="text" 
                            className="validate"
                            style={{color:"white"}}
                            value={this.state.username}
                            onChange={this.updateUserName}/>
                    </div>
                    <a className={"waves-effect waves-light btn-large"+ (this.state.username ===''? " disabled": "")}
                        onClick={this.submitResults}
                        rel="noopener noreferrer"
                    > Submit
                    </a>
                    </div>
                :
                <div>
                    <h2>You scored {this.calculateScore()} / {this.state.questions.length} </h2>
                    <a className="waves-effect waves-light btn-large"
                        href="/"
                        style={{margin:"5px"}}
                        rel="noopener noreferrer"
                        >Retry
                        </a>
                        <a className="waves-effect waves-light btn-large"
                        href="/standings"
                        style={{margin:"5px"}}
                        rel="noopener noreferrer"
                        >Leaderboard
                        </a>
                </div>
                }
                               
            </div>
        )
    }
}


export default Results;