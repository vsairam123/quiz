import React from 'react';
import axios from 'axios';
// import questionService from './questionService';
import Question from './Question';
import Results from './Results';

const API_URL = "https://quiz-udemy-backend.herokuapp.com/"

class Quiz extends React.Component{

    constructor(props){
        super(props);
        this.state={
            questionBank : [],
            selectedAnswers:[], 
            questionCounter:0,
            question:'',
            options:'',
            numQuestions : 0
        }

        this.getQuestions= this.getQuestions.bind(this);
        this.nextQuestion = this.nextQuestion.bind(this);
    }

    nextQuestion= (option) => {
        let currQuestion = this.state.questionCounter;
        let answers = [...this.state.selectedAnswers,option]
        currQuestion++;
        this.setState({questionCounter:currQuestion,selectedAnswers:answers})
    }
    
    getQuestions = (quizname) => {
        // questionService.then((data)=> this.setState({questionBank:data,numQuestions:data.length} ));        
        const url = API_URL+"api/getQuestions/"+quizname;
        axios.get(url,{headers: {
            'Access-Control-Allow-Origin': '*',
          }
        }).then((response)=>{
            let data = response.data[0].questions;
            this.setState({questionBank:data,numQuestions:data.length} )
        });
    
    }

    componentDidMount(){
        this.getQuestions("quiz1");
    }
 
    render(){
        let quesNo = this.state.questionCounter;
        let q = this.state.questionBank[quesNo];

        return (
            <div className="App">
                <h1>Quiz</h1>
                {this.state.questionBank.length > this.state.questionCounter ? 
                <Question 
                    bank = {q}
                    qno = {quesNo}
                    nextQuestion = {this.nextQuestion}
                    numQuestions = {this.state.numQuestions}
                    /> : 
                <Results
                    bank={this.state.questionBank}
                    selectedAnswers={this.state.selectedAnswers}
                    
                /> }
                
            </div>
        )
    }
}


export default Quiz ;