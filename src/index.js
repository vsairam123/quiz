import React from 'react';
import ReactDOM from 'react-dom';
import { Route, BrowserRouter as Router } from 'react-router-dom';

import './index.css';
import App from './App';
import Quiz from './components/Quiz';
import Leaderboard from './components/Leaderboard';
import * as serviceWorker from './serviceWorker';

const routing = (
  <Router>
      <div className="quiz">
        <Route exact path = "/" component={App}/>
        <Route path = "/quiz/:quizname" component={Quiz}/>
        <Route path = "/standings" component={Leaderboard}/>  
      </div>
  </Router>
);

ReactDOM.render(
  routing,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
